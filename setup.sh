#!/usr/bin/env bash

apt-get -y install cppcheck
apt-get -y install nodejs npm

apt-get -y install doxygen graphviz

npm install -g particle-cli
