#!/usr/bin/env bash

set -e

TMPDIR=tmp

if [ "x" == "x$1" ]; then
  echo "usage: $0 <cpp-file>"
  exit 1
fi

CPPFILE=$1
echo checking $CPPFILE

cppcheck --enable=all --error-exitcode=1 --inline-suppr $CPPFILE
